#[macro_use]
extern crate log;

use crate::resolver::Resolver;
use crate::watcher::Watcher;
use log::LevelFilter;
use std::{thread, time};

mod resolver;
mod watcher;

pub type Result<T> = std::result::Result<T, anyhow::Error>;

fn main() -> Result<()> {
    env_logger::builder().filter_level(LevelFilter::Debug).init();

    let filename = "tests/dhcp.leases";
    let resolver = Resolver::new(filename)?;
    let mut watcher = Watcher::new();

    watcher.watch(filename, resolver)?;

    thread::sleep(time::Duration::from_secs(5));

    Ok(())
}
