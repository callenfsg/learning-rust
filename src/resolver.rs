use crate::watcher::WatchEvents;
use log::error;
use std::borrow::ToOwned;
use std::collections::HashMap;
use std::fs::File;
use std::io::{self, BufRead};

#[derive(PartialEq, Debug)]
pub struct Device {
    pub mac_address: String,
    pub ip_address: String,
    pub description: String,
}

pub struct Resolver {
    pub filename: String,
    devices: HashMap<String, Device>,
}

impl Resolver {
    pub fn new(filename: &str) -> io::Result<Self> {
        let mut r = Self {
            filename: filename.to_owned(),
            devices: HashMap::new(),
        };
        r.load()?;
        Ok(r)
    }

    pub fn list_devices(&self) -> Vec<&Device> {
        self.devices.values().collect()
    }

    pub fn load(&mut self) -> io::Result<()> {
        let f = File::open(self.filename.as_str())?;
        let reader = io::BufReader::new(f);

        for line in reader.lines() {
            if let Ok(ln) = line {
                let split: Vec<&str> = ln.split(' ').collect();
                match self.devices.get_mut(split[1]) {
                    Some(device) => {
                        device.ip_address = split[2].to_owned();
                        device.description = split[3].to_owned();
                    }
                    None => {
                        let device = Device {
                            mac_address: split[1].to_owned(),
                            ip_address: split[2].to_owned(),
                            description: split[3].to_owned(),
                        };
                        self.devices.insert(device.mac_address.clone(), device);
                    }
                }
            }
        }

        Ok(())
    }
}

impl WatchEvents for Resolver {
    fn on_update(&mut self) {
        match self.load() {
            Ok(_) => {}
            Err(e) => {
                error!("{}", e);
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn new_loads_dhcp_file() {
        let r = Resolver::new("tests/dhcp.leases").unwrap();
        assert_eq!(
            Device {
                mac_address: String::from("8c:85:90:a1:08:63"),
                ip_address: String::from("192.168.8.121"),
                description: String::from("sirius")
            },
            r.devices["8c:85:90:a1:08:63"]
        )
    }

    #[test]
    fn list_devices() {
        let r = Resolver::new("tests/dhcp.leases").unwrap();
        assert_eq!(
            vec![&Device {
                mac_address: String::from("8c:85:90:a1:08:63"),
                ip_address: String::from("192.168.8.121"),
                description: String::from("sirius")
            }],
            r.list_devices()
        )
    }
}
