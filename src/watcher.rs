use crate::Result;
use filetime::FileTime;
use std::collections::HashMap;
use std::path::{Path, PathBuf};
use std::sync::{Arc, Mutex};
use std::time::Duration;
use std::{fs, thread};

pub trait WatchEvents {
    fn on_update(&mut self) {}
}

struct Watch {
    path: PathBuf,
    receiver: Box<dyn WatchEvents + Send + Sync>,
    mtime: i64,
}

pub struct Watcher {
    delay: Duration,
    watches: Arc<Mutex<HashMap<PathBuf, Watch>>>,
}

impl Watcher {
    pub fn new() -> Self {
        let mut w = Self {
            delay: Duration::from_secs(2),
            watches: Arc::new(Mutex::new(HashMap::new())),
        };
        w.run();
        w
    }

    pub fn watch<P, R>(&mut self, path: P, receiver: R) -> Result<()>
    where
        P: AsRef<Path>,
        R: 'static + WatchEvents + Send + Sync,
    {
        let watch = Watch {
            path: path.as_ref().to_path_buf(),
            receiver: Box::new(receiver),
            mtime: 0,
        };
        if let Ok(mut watches) = self.watches.lock() {
            watches.insert(watch.path.clone(), watch);
        }
        Ok(())
    }

    fn run(&mut self) {
        let watches = self.watches.clone();
        let delay = self.delay;

        thread::spawn(move || loop {
            thread::sleep(delay);

            if let Ok(mut watches) = watches.lock() {
                for (_, watch) in watches.iter_mut() {
                    match fs::metadata(watch.path.as_path()) {
                        Err(e) => {
                            let err_path = watch.path.to_str().unwrap();
                            error!("Unable to fetch metadata for path {}: {}", err_path, e);
                            continue;
                        }
                        Ok(metadata) => {
                            if metadata.is_dir() {
                                let err_path = watch.path.to_str().unwrap();
                                error!(
                                    "Directories are not supported; expected a single file but got: {}",
                                    err_path
                                );
                                continue;
                            }

                            let mtime = FileTime::from_last_modification_time(&metadata).seconds();
                            if mtime > watch.mtime {
                                watch.mtime = mtime;
                                watch.receiver.on_update();
                            }
                        }
                    }
                }
            }
        });
    }
}
